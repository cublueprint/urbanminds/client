export const displayDateTime = (datetimeString) => {
  const options: Intl.DateTimeFormatOptions = {
    weekday: 'short',
    month: 'long',
    day: 'numeric',
    year: 'numeric',
    timeZoneName: 'short',
    hour: 'numeric',
    minute: 'numeric',
  }
  const date = new Date(datetimeString)

  const formatted = date.toLocaleString('en-US', options);
  return formatted;
};