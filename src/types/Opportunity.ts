export type Opportunity = {
  id: string;
  title: string;
  description: string;
  eventLink: string;
  startDate: Date;
  image: {
    key: string;
    location: string;
  } | undefined;
  location: string;
  school: string;
  points: number;
  capacity: number;
  organizationName: string;
  eventType: string;
  code: string | undefined;
  createdAt: string;
  updatedAt: string;
  registered: boolean;
  users: [any] | undefined;
};
