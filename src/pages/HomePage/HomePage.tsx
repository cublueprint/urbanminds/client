import { useAuth } from '../../hooks/useAuth';
import { Navigate } from 'react-router-dom';

export function HomePage() {
  const [user] = useAuth();

  if (user) {
    return <Navigate to="/dashboard" />;
  } else {
    return <Navigate to="/login" />;
  }
}
