import * as React from 'react';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import LinearProgress, {
  linearProgressClasses,
} from '@mui/material/LinearProgress';
import Box from '@mui/material/Box';
import { FormGroup, FormControlLabel, Switch } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider, styled } from '@mui/material/styles';
import { Navigate, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { FormHelperText, TextField } from '@mui/material';
import { SearchNavBar } from '../../components/SearchNavBar';
import { Opportunity } from '../../types/Opportunity';
import { useAuth } from '../../hooks/useAuth';
import { Footer } from '../../components/Footer';

const theme = createTheme({
  palette: {
    text: {
      primary: '#1A1A1A',
      secondary: '#B2B2B2',
    },
    background: {
      default: '#F6F6F6',
    },
  },
  typography: {
    fontFamily: "'Helvetica Neue', Helvetica, Arial, sans-serif",
  },
});

function formatDate(date) {
  const formattedDate = new Date(date).toLocaleDateString(undefined, {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  });
  return formattedDate;
}

export function ProfilePage() {
  const [user, _, logout] = useAuth();
  const navigate = useNavigate();
  let query = '';
  const [searchQuery, setSearchQuery] = useState<string>(''); // add search query state variable
  const [active, setActive] = useState<boolean>(false);
  const [location, setLocation] = useState('');
  const accessToken = user?.tokens?.access.token;
  const [origOpps, setOrigOpps] = useState([]); // stores the original opportunities so that searches can be backtracked
  const [opps, setOpps] = useState<Opportunity[]>([]);

  const [points, setPoints] = useState(user ? user.points : 0);
  const [isClicked, setIsClicked] = useState<Opportunity | undefined>(undefined);
  let urlEndPoint = `${process.env.REACT_APP_API_ENDPOINT}/opportunities?active=${active}`;

  const filterPosts = (posts, query) => {
    if (query === '') {
      return posts;
    }
    return posts.filter(post => {
      const postName = post.title.toLowerCase();
      return postName.includes(query);
    });
  };

  if (!user || !user.tokens) {
    return <Navigate to="/login" />;
  }

  const getChoice = choice => {
    query = choice;
    setOpps(filterPosts(origOpps, query));
    console.log(opps);
  };

  const fetchProfileData = () => {
    const values = {};
    const requestOptions: RequestInit = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
      },
      body: JSON.stringify(values),
    };
    
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/users/${user.id}`, requestOptions)
      .then(async response => {
        if (!response.ok) {
          return response.json().then(data => {
            throw new Error(data.message);
          });
        } else {
          return response.json();
        }
      })
      .then(data => {
      })
      .catch(err => {
        console.log(err.message);
      });
  };


  return (
    <ThemeProvider theme={theme}>
      <title>User Profile</title>
      <CssBaseline />
      <SearchNavBar giveChoice={getChoice} />
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: 'background.default',
            pt: 8,
            pb: 6,
          }}
        >
          <Container maxWidth="xl">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Profile
            </Typography>
            <Grid
              container
              my={3}
              spacing={4}
              direction="column"
              justifyContent="center"
              alignItems="center"
            >
              <Grid item xs={4}>
              <Card
                sx={{
                    height: '450px',
                    width: '1050px',
                    p: 5,
                    display: 'flex',
                }}
                >
                    <Grid container spacing={2}>
                        <Grid item xs={4}>
                        <Avatar
                            alt="Avatar"
                            src="avatar.png"
                            sx={{ width: 140, height: 140, ml: 10 }}
                        />
                        </Grid>
                        <Grid item xs={8}>
                        <div>
                          <Typography variant="h6" sx={{ fontSize: 20, mt: 2 }}>
                            <strong>Name:</strong> {user.firstName} {user.lastName}
                            </Typography>

                            <Typography variant="h6" sx={{ fontSize: 20, mt: 2 }}>
                            <strong>Email:</strong> {user.email}
                            </Typography>

                            <Typography variant="h6" sx={{ fontSize: 20, mt: 2 }}>
                            <strong>Birthday:</strong> {formatDate(user.birthDate)} 
                            </Typography>

                            <Typography variant="h6" sx={{ fontSize: 20, mt: 2 }}>
                            <strong>Account created on:</strong> {formatDate(user.createdAt)}
                            </Typography>
                            
                            <Typography variant="h6" sx={{ fontSize: 20, mt: 2 }}>
                            <strong>Role:</strong> {user.role === 'user' ? 'Student' : user.role === 'admin' ? 'Admin' : user.role}
                            </Typography>

                            <Typography variant="h6" sx={{ fontSize: 20, mt: 2 }}>
                            <strong>Points:</strong> {user.points}
                            </Typography>
                        </div>
                        </Grid>
                    </Grid>
                </Card>
              </Grid>
          </Grid>
        </Container >
        </Box >
      </main >
      <Footer />
    </ThemeProvider >
  );
}