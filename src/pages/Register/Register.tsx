import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormGroup from '@mui/material/FormGroup';
import FormLabel from '@mui/material/FormLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { Alert, AlertTitle,  } from '@mui/material';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Divider from '@mui/material/Divider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import SchoolIcon from '@mui/icons-material/School';
import OutlinedInput from '@mui/material/OutlinedInput';
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined';
import VisibilityOffOutlinedIcon from '@mui/icons-material/VisibilityOffOutlined';
import IconButton from '@mui/material/IconButton';
import InputLabel from '@mui/material/InputLabel';
import ReactInputVerificationCode from 'react-input-verification-code';
import FormControl from '@mui/material/FormControl';
import { PasswordStrengthBar } from '../../components/PasswordStrengthBar';
import { PasswordStrengthIndicator } from '../../components/PasswordStrengthIndicator';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { useAuth } from '../../hooks/useAuth';
import { Navigate } from 'react-router-dom';
import { PhoneOutlined } from '@mui/icons-material';
import { ErrorText, StyledButton } from '../../components/ErrorText';

interface RegisterAccount {
  email: string;
  phoneNumber: string,
  password: string;
  confirmPassword: string;
  firstName: string;
  lastName: string;
  school: string;
  birthday: Date | null;
  involvement: { [key: string]: boolean };
  showPassword: boolean;
}

/** TODO: NEED TO MAKE A THEME COMPONENT  */
const theme = createTheme({
  palette: {
    text: {
      primary: '#1A1A1A',
      secondary: '#B2B2B2',
    },
  },
  typography: {
    fontFamily: "'Helvetica Neue', Helvetica, Arial, sans-serif",
  },
  
});

const pastInvolvementOptions = {
  torontoConference: '1UP Toronto Conference',
  leadersLab: '1UP Leaders Lab',
  chapters: '1UP Chapters',
  designCompetition: '1UP Design Competition',
  officeTours: '1UP Office Tours',
};

export function Register() {

  const [user, login] = useAuth();
  const [status, setStatus] = React.useState('');
  const [submitError, setSubmitError] = React.useState('');
  // password states
  const [showPassword, setShowPassword] = React.useState(false);

  const [passwordFocused, setPasswordFocused] = React.useState(false);

  const [passwordValidity, setPasswordValidity] = React.useState({
    minChar: false,
    number: false,
    lower: false,
    upper: false,
    specialChar: false,
  });
  // TODO: NEXT BUTTON SHOULD ALSO MAKE SURE THE FORM IS VALIDATED
  const [page, setPage] = React.useState(1);

  // const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
  //   event.preventDefault();
  //   const data = new FormData(event.currentTarget);
  //   console.log(data);
  // };

  const isNumberRegx = /\d/;
  const isLower = /[a-z]/;
  const isUpper = /[A-Z]/;
  const specialCharacterRegx = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
  const onChangePassword = password => {
    setPasswordValidity({
      minChar: password.length >= 8 ? true : false,
      number: isNumberRegx.test(password) ? true : false,
      lower: isLower.test(password) ? true : false,
      upper: isUpper.test(password) ? true : false,
      specialChar: specialCharacterRegx.test(password) ? true : false,
    });
  };
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault();
  };

  if (user) {
    return <Navigate to="/dashboard" />;
  }

  return (
    <ThemeProvider theme={theme}>
        <title>Register</title>
      {status == 'success' && (
        <Alert severity="success">
          <AlertTitle>Success</AlertTitle>
          Your account has been created !
          <strong>Redirecting to login page. . .</strong>
        </Alert>
      )}
      <Grid
        container
        component="main"
        sx={{ display: 'flex', height: '100%', minHeight: '100vh' }}
      >
        <CssBaseline />
        <Grid
          item
          sx={{ px: 9, py: '8vh', flexShrink: 0, width: 590, height: '100%' }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            {/* logo - CHANGED */}
            <img
              src="1UPLogo.png"
              alt="logo"
              style={{
                height: '15%',
                width: '20%',
                marginBottom: '5%',
              }}
            />

            <Typography
              variant="h4"
              className="inter-heading"
              sx={{
                fontFamily: 'Inter',
                fontWeight: 600,
                fontSize: '36px',
                lineHeight: '44px',
                my: 1.5,
              }}
            >
              Register
            </Typography>
            <Typography
              sx={{
                width: '313px',
                height: '44px',
                fontSize: '16px',
                lineHeight: '138%',
                color: '#B3B3B3',
                my: 1,
              }}
            >
              Youth of today. Changemakers of tomorrow. Join the community.
            </Typography>
            <Divider light sx={{ mt: 2, mb: 1 }} />
            <Formik
              initialValues={{
                email: '',
                phoneNumber: '',
                password: '',
                confirmPassword: '',
                firstName: '',
                lastName: '',
                school: '',
                birthday: null,
                involvement: Object.fromEntries(
                  Object.entries(pastInvolvementOptions).map(([k, v]) => [
                    k,
                    false,
                  ]),
                ),
                showPassword: false,
              }}
              validationSchema={Yup.object().shape({
                firstName: Yup.string().required('First name is required'),
                lastName: Yup.string().required('Last name is required'),
                email: Yup.string()
                  .email('Email is not valid')
                  .required('Email is required'),
                phoneNumber: Yup.string()
                  .matches(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/, 'Phone number is not valid'),
                school: Yup.string(),
                birthday: Yup.date()
                  .nullable()
                  .typeError('Invalid date entered')
                  .required('Birthday is required')
                  .max(
                    new Date(
                      new Date().setFullYear(new Date().getFullYear() - 10),
                    ),
                    'Must be at least 10 years old',
                  ),
                password: Yup.string()
                  .required('Password is required')
                  .min(8, 'Must be 8 characters or more')
                  .matches(/[a-z]+/, 'One lowercase character')
                  .matches(/[A-Z]+/, 'One uppercase character')
                  .matches(
                    /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/,
                    'One special character',
                  )
                  .matches(/\d+/, 'One number'),
                confirmPassword: Yup.string()
                  .equals([Yup.ref('password')], 'Passwords do not match.')
                  .required('Re-enter your password.'),
              })}
              // SUBMIT
              onSubmit={async (values: RegisterAccount, { setSubmitting }) => {
                let data = {
                  firstName: values.firstName,
                  lastName: values.lastName,
                  phoneNumber: values.phoneNumber,
                  school: values.school,
                  email: values.email,
                  birthDate: values.birthday,
                  password: values.password,
                  pastInvolvement: Object.entries(values.involvement)
                    .filter(([_, v]) => v)
                    .map(([k, v]) => pastInvolvementOptions[k])
                    .join(', '),
                };
                const requestOptions = {
                  method: 'POST',
                  headers: { 'Content-Type': 'application/json' },
                  body: JSON.stringify(data),
                };
                fetch(`${process.env.REACT_APP_API_ENDPOINT}/auth/register`, requestOptions)
                  .then(async response => {
                    if (!response.ok) {
                      let res = await response.json();
                      throw new Error(res.message);
                    } else {
                      setStatus('success');
                      return response.json();
                    }
                  })
                  .then(data => {
                    login(data);
                  })
                  .catch(err => {
                    console.log(err.message);
                    setSubmitError(err.message);
                    setSubmitting(false);
                  });
              }}
            >
              {props => {
                const {
                  values,
                  touched,
                  errors,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  setFieldValue,
                  validateForm,
                  setTouched,
                  isValid,
                } = props;
                return (
                  <Form onSubmit={handleSubmit} className="form">
                    {/* register form  page 1  */}
                    {page == 1 && (
                      <Box sx={{ mt: 1 }}>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={6}>
                            <TextField
                              name="firstName"
                              fullWidth
                              id="firstName"
                              label="First Name"
                              error={Boolean(
                                errors.firstName && touched.firstName,
                              )}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.firstName}
                            />
                            <ErrorText
                              error={touched.firstName ? errors.firstName : ''}
                            />
                          </Grid>
                          <Grid item xs={12} sm={6}>
                            <TextField
                              fullWidth
                              id="lastName"
                              label="Last Name"
                              name="lastName"
                              error={Boolean(
                                errors.lastName && touched.lastName,
                              )}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.lastName}
                            />
                            <ErrorText
                              error={touched.lastName ? errors.lastName : ''}
                            />
                          </Grid>
                        </Grid>
                        <TextField
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <MailOutlineIcon />
                              </InputAdornment>
                            ),
                          }}
                          margin="normal"
                          fullWidth
                          id="email"
                          label="Email Address"
                          name="email"
                          error={Boolean(errors.email && touched.email)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.email}
                        />
                        <ErrorText error={touched.email ? errors.email : ''} />
                        <TextField
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <PhoneOutlined />
                              </InputAdornment>
                            ),
                          }}
                          margin="normal"
                          fullWidth
                          id="phoneNumber"
                          label="Phone Number"
                          name="phoneNumber"
                          error={Boolean(errors.phoneNumber && touched.phoneNumber)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.phoneNumber}
                        />
                        <ErrorText error={touched.phoneNumber ? errors.phoneNumber : ''} />
                        <TextField
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">
                                <SchoolIcon />
                              </InputAdornment>
                            ),
                          }}
                          variant="outlined"
                          margin="normal"
                          fullWidth
                          id="school"
                          name="school"
                          label="School"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.school}
                        />
                        <ErrorText
                          error={touched.school ? errors.school : ''}
                        />
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <DatePicker
                            label="Birthday"
                            onChange={value => setFieldValue('birthday', value)}
                            value={values.birthday}
                            slotProps={{ textField: {
                                name: "birthday",
                                fullWidth: true,
                                margin: "normal",
                                required: true,
                                error: Boolean(
                                  errors.birthday && touched.birthday,
                                ),
                                onBlur: handleBlur,
                              }
                            }}
                          />
                        </LocalizationProvider>
                        <ErrorText
                          error={touched.birthday ? errors.birthday : ''}
                        />
                        <FormGroup sx={{ mt: 2, mb: 2 }}>
                          <FormLabel>Have you been involved in any of the following past 1UP events?</FormLabel>
                          {Object.keys(pastInvolvementOptions).map(key => (
                            <FormControlLabel
                              control={
                                <Checkbox
                                  name={`involvement.${key}`}
                                  checked={values.involvement[key]}
                                  onChange={handleChange}
                                />
                              }
                              label={
                                <Typography color="text.secondary">
                                  {pastInvolvementOptions[key]}
                                </Typography>
                              }
                              sx={{ mb: -1 }}
                            />
                          ))}
                        </FormGroup>
                        <NextButton
                          onClick={() => {
                            setTouched(
                              {
                                firstName: true,
                                lastName: true,
                                phoneNumber: true,
                                email: true,
                                school: true,
                                birthday: true,
                              },
                              false,
                            );

                            validateForm().then(result => {
                              if (
                                !(
                                  result.firstName ||
                                  result.lastName ||
                                  result.phoneNumber ||
                                  result.email ||
                                  result.school ||
                                  result.birthday
                                )
                              ) {
                                setPage(2);
                                setSubmitError('');
                              }
                            });
                          }}
                        />
                        <Grid
                          container
                          sx={{ justifyContent: 'space-between' }}
                        >
                          <Grid item sx={{ mr: 2 }}>
                            <Link
                              href="/login"
                              variant="body2"
                              className="blue-string"
                              sx={{ textDecoration: 'none' }}
                            >
                              {'Already have an account? Sign in.'}
                            </Link>
                          </Grid>
                        </Grid>
                      </Box>
                    )}

                    {/* register page 2 TODO: */}
                    {page == 2 && (
                      <Box sx={{ mt: 1 }}>
                        <Typography
                          sx={{
                            width: '430px',
                            height: '66px',
                            fontSize: '16px',
                            lineHeight: '138%',
                            color: '#B3B3B3',
                            my: 1,
                          }}
                        >
                          Hi {values.firstName}, set a password for your account
                          below. Remember, strong passwords include a
                          combination of letters, numbers, and special
                          characters.
                        </Typography>
                        <PasswordInput
                          name="password"
                          labelName="Password"
                          showPassword={showPassword}
                          value={values.password}
                          handleChange={e => {
                            onChangePassword(e.target.value);
                            handleChange(e);
                          }}
                          handleFocus={() => setPasswordFocused(true)}
                          error={Boolean(errors.password && touched.password)}
                          handleBlur={handleBlur}
                          handleClickShowPassword={handleClickShowPassword}
                          handleMouseDownPassword={handleMouseDownPassword}
                        />
                        <ErrorText
                          error={touched.password ? errors.password : ''}
                        />
                        <PasswordInput
                          name="confirmPassword"
                          labelName="Confirm password"
                          showPassword={showPassword}
                          value={values.confirmPassword}
                          handleChange={handleChange}
                          error={Boolean(
                            errors.confirmPassword && touched.confirmPassword,
                          )}
                          handleBlur={handleBlur}
                          handleFocus={() => setPasswordFocused(true)}
                          handleClickShowPassword={handleClickShowPassword}
                          handleMouseDownPassword={handleMouseDownPassword}
                        />
                        <PasswordStrengthBar
                          password={values.password}
                          validity={passwordValidity}
                        />
                        <ErrorText
                          error={
                            touched.confirmPassword
                              ? errors.confirmPassword
                              : ''
                          }
                        />
                        <Box color="text.secondary" my={5}>
                          {passwordFocused && (
                            <PasswordStrengthIndicator
                              validity={passwordValidity}
                            />
                          )}
                        </Box>
                        <SubmitButton
                          mb={5}
                          disabled={
                            isSubmitting
                          }
                        />
                        {submitError && (
                          <>
                            <Typography my={3} color="error">
                              {' '}
                              ERROR: {submitError}{' '}
                            </Typography>
                            <StyledButton variant="text" onClick={() => setPage(1)}>
                              Go back to register
                            </StyledButton>
                          </>
                        )}
                      </Box>
                    )}
                  </Form>
                );
              }}
            </Formik>
          </Box>
        </Grid>

        {/* Image Grid,  TODO: NEED TO MAKE IT A SEPERATE COMPONENT */}
        <Grid
          item
          xs={true}
          sm={true}
          md={true}
          xl={true}
          sx={{
            backgroundImage:
              'url(https://i.ibb.co/6wxXxLH/1-UP-Conference-Sign-up-Image.jpg)',
            backgroundRepeat: 'no-repeat',
            backgroundColor: t =>
              t.palette.mode === 'light'
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            flexShrink: 1,
          }}
        />
      </Grid>
    </ThemeProvider>
  );
}

const PasswordInput = ({
  name,
  labelName,
  showPassword,
  value,
  handleChange,
  error,
  handleBlur,
  handleFocus,
  handleClickShowPassword,
  handleMouseDownPassword,
}) => {
  return (
    <FormControl required margin="normal" fullWidth variant="outlined">
      <InputLabel>{labelName}</InputLabel>
      <OutlinedInput
        id={'outlined-adornment-' + name}
        name={name}
        type={showPassword ? 'text' : 'password'}
        value={value}
        onChange={handleChange}
        error={error}
        onBlur={handleBlur}
        onFocus={handleFocus}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              onMouseDown={handleMouseDownPassword}
              edge="end"
            >
              {showPassword ? (
                <VisibilityOffOutlinedIcon />
              ) : (
                <VisibilityOutlinedIcon />
              )}
            </IconButton>
          </InputAdornment>
        }
        label={labelName}
      />
    </FormControl>
  );
};

const NextButton = (props: any) => {
  return (
    <Button
      {...props}
      fullWidth
      variant="contained"
      sx={{
        backgroundColor: '#1A1A1A',
        my: 2,
        height: '50px',
        '&:hover': {
          backgroundColor: '#333',
        },
      }}
    >
      Next
    </Button>
  );
};

const SubmitButton = (props: any) => {
  return (
    <Button
      {...props}
      fullWidth
      variant="contained"
      type="submit"
      sx={{
        backgroundColor: '#1A1A1A',
        my: 2,
        height: '50px',
        '&:hover': {
          backgroundColor: '#333',
        },
      }}
    >
      Complete registration
    </Button>
  );
};
