import React from 'react';
import { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import InputAdornment from '@mui/material/InputAdornment';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Divider from '@mui/material/Divider';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Navigate } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';


type Account = {
  email: string;
  password: string;
};

// TODO: NEED TO MAKE THEME A COMPONENT
const theme = createTheme({
  palette: {
    text: {
      primary: '#1A1A1A',
      secondary: '#B2B2B2',
    },
  },
  typography: {
    fontFamily: "'Helvetica Neue', Helvetica, Arial, sans-serif",
  },
});

export function LoginPage() {
  const [user, login] = useAuth();
  if (user) {
    return <Navigate to="/dashboard" />;
  }

  return (
    <ThemeProvider theme={theme}>
      <title>Login</title>
      <Grid
        container
        component="main"
        sx={{ display: 'flex', height: '100%', minHeight: '100vh' }}
      >
        <CssBaseline />

        {/* Left Grid: Sign in form */}
        <Grid
          item
          sx={{ px: 9, py: '8vh', flexShrink: 0, width: 590, height: '100%' }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            {/* logo - CHANGED */}
            <img
              src="1UPLogo.png"
              alt="logo"
              style={{
                height: '15%',
                width: '20%',
                marginBottom: '5%',
              }}
            />

            <Typography
              variant="h4"
              className="inter-heading"
              sx={{
                fontFamily: 'Inter',
                fontWeight: 600,
                fontSize: '36px',
                lineHeight: '44px',
                my: 1.5,
              }}
            >
              Sign In
            </Typography>
            <Typography
              variant="caption"
              sx={{
                width: '313px',
                height: '44px',
                fontSize: '16px',
                lineHeight: '138%',
                color: '#B3B3B3',
                my: 1,
              }}
            >
              Youth of today. Changemakers of tomorrow. Join the community.
            </Typography>
            <Divider light sx={{ mt: 2, mb: 1 }} />
            <Formik
              initialValues={{
                email: '',
                password: '',
              }}
              onSubmit={async (
                values: Account,
                { setSubmitting, setFieldError, setFieldTouched },
              ) => {
                console.log(JSON.stringify(values));
                const requestOptions: RequestInit = {
                  method: 'POST',
                  headers: { 'Content-Type': 'application/json' },
                  body: JSON.stringify(values),
                };
                fetch(`${process.env.REACT_APP_API_ENDPOINT}/auth/login`, requestOptions)
                  .then(async response => {
                    if (!response.ok) {
                      let res = await response.json();
                      throw new Error(res.message);
                    } else {
                      return response.json();
                    }
                  })
                  .then(data => {
                    login(data);
                  })
                  .catch(err => {
                    console.log(err);
                    setFieldTouched('email', true, false);
                    setFieldTouched('password', true, false);
                    setFieldError('email', err.message);
                    setFieldError('password', err.message);
                    setSubmitting(false);
                  });
              }}
              validationSchema={Yup.object().shape({
                email: Yup.string()
                  .email('Please enter a valid email.')
                  .required('Email is required.'),
                password: Yup.string().required('Password is required.'),
              })}
            >
              {props => {
                const {
                  values,
                  touched,
                  errors,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;

                return (
                  <form onSubmit={handleSubmit}>
                    <TextField
                      id="email"
                      name="email"
                      label="Email Address"
                      variant="outlined"
                      margin="normal"
                      fullWidth
                      value={values.email}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      error={Boolean(errors.email && touched.email)}
                      helperText={touched.email ? errors.email : ''}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <MailOutlineIcon />
                          </InputAdornment>
                        ),
                      }}
                    />
                    <TextField
                      id="password"
                      name="password"
                      label="Password"
                      variant="outlined"
                      type="password"
                      margin="normal"
                      fullWidth
                      value={values.password}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      error={Boolean(errors.password && touched.password)}
                      helperText={touched.password ? errors.password : ''}
                    />
                    <FormControlLabel
                      control={<Checkbox value="remember" />}
                      label={
                        <Typography color="text.secondary">
                          Remember me
                        </Typography>
                      }
                    />
                    {/* TODO STYLE BUTTON */}
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      disabled={
                        isSubmitting ||
                        !Boolean(
                          !Boolean(errors.email || errors.password) &&
                          values.email &&
                          values.password,
                        )
                      }
                      sx={{
                        backgroundColor: '#1A1A1A',
                        my: 2,
                        height: '50px',
                        '&:hover': {
                          backgroundColor: '#333',
                        },
                        '&:disabled': {
                          backgroundColor: '#1A1A1A',
                          color: '#FFFFFF',
                        },
                      }}
                    >
                      Sign In
                    </Button>
                    <Grid container sx={{ justifyContent: 'space-between' }}>
                      <Grid item sx={{ mr: 2 }}>
                        <Link
                          href="/register"
                          variant="body2"
                          className="blue-string"
                          sx={{ textDecoration: 'none' }}
                        >
                          {"Don't have an account? Create One"}
                        </Link>
                      </Grid>
                      <Grid item>
                        <Link
                          href="#"
                          variant="body2"
                          className="blue-string"
                          sx={{ textDecoration: 'none' }}
                        >
                          Forgot password?
                        </Link>
                      </Grid>
                    </Grid>
                  </form>
                );
              }}
            </Formik>
          </Box>
        </Grid>

        {/* Image Grid,  TODO: NEED TO MAKE IT A SEPERATE COMPONENT */}
        <Grid
          item
          xs={true}
          sm={true}
          md={true}
          xl={true}
          sx={{
            backgroundImage:
              'url(https://i.ibb.co/6wxXxLH/1-UP-Conference-Sign-up-Image.jpg)',
            backgroundRepeat: 'no-repeat',
            backgroundColor: t =>
              t.palette.mode === 'light'
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            flexShrink: 1,
          }}
        />
      </Grid>
    </ThemeProvider >
  );
}
