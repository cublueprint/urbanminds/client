import * as React from 'react';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import { Link } from 'react-router-dom';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import LinearProgress, {
  linearProgressClasses,
} from '@mui/material/LinearProgress';
import Box from '@mui/material/Box';
import { FormGroup, FormControlLabel, Switch } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider, styled } from '@mui/material/styles';
import { Navigate, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { FormHelperText, TextField } from '@mui/material';
import { SearchNavBar } from '../../components/SearchNavBar';
import { Opportunity } from '../../types/Opportunity';
import { useAuth } from '../../hooks/useAuth';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from '@mui/material';
import { Footer } from '../../components/Footer';


const theme = createTheme({
  palette: {
    text: {
      primary: '#1A1A1A',
      secondary: '#B2B2B2',
    },
    background: {
      default: '#F6F6F6',
    },
  },
  typography: {
    fontFamily: "'Helvetica Neue', Helvetica, Arial, sans-serif",
  },
});

function formatDate(date) {
  const formattedDate = new Date(date).toLocaleDateString(undefined, {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  });
  return formattedDate;
}

interface User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  createdAt: string; 
  points: number;
  role: string; 
}

export function UsersListPage() {
  const [user, _, logout] = useAuth();
  const navigate = useNavigate();
  let query = '';
  const [searchQuery, setSearchQuery] = useState<string>(''); // add search query state variable
  const [active, setActive] = useState<boolean>(false);
  const [location, setLocation] = useState('');
  const accessToken = user?.tokens?.access.token;
  const [origOpps, setOrigOpps] = useState([]); // stores the original opportunities so that searches can be backtracked
  const [opps, setOpps] = useState<Opportunity[]>([]);

  const [userList, setUserList] = useState([]); // stores the list of users

  const [points, setPoints] = useState(user ? user.points : 0);
  const [isClicked, setIsClicked] = useState<Opportunity | undefined>(undefined);
  let urlEndPoint = `${process.env.REACT_APP_API_ENDPOINT}/opportunities?active=${active}`;

  const filterPosts = (posts, query) => {
    if (query === '') {
      return posts;
    }
    return posts.filter(post => {
      const postName = post.title.toLowerCase();
      return postName.includes(query);
    });
  };

  if (!user || !user.tokens) {
    return <Navigate to="/login" />;
  }

  const getChoice = choice => {
    query = choice;
    setOpps(filterPosts(origOpps, query));
    console.log(opps);
  };

  // Fetch the list of users when the component mounts
  const fetchUserList = () => {
    urlEndPoint = `${process.env.REACT_APP_API_ENDPOINT}/users`;

    fetch(urlEndPoint, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    })
      .then(async response => {
        if (!response.ok) {
          let res = await response.json();
          throw new Error(res.message);
        } else {
          return response.json();
        }
      })
      .then(data => {
        setUserList(data.results);
      })
      .catch(err => {
        logout();
        console.log(err.message);
      });
  }
  
  useEffect(fetchUserList, []);

  return (
    <ThemeProvider theme={theme}>
      <title>All Users</title>
      <CssBaseline />
      <SearchNavBar giveChoice={getChoice} />
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: 'background.default',
            pt: 8,
            pb: 6,
          }}
        >
          <Container maxWidth="xl">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              All Users
            </Typography>
            
            <Grid container my={3} spacing={4} direction="column" justifyContent="center" alignItems="center">
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell> <strong> User Name </strong></TableCell>
                      <TableCell> <strong> Email </strong></TableCell>
                      <TableCell> <strong> Points </strong></TableCell>
                      <TableCell> <strong> Role </strong></TableCell>
                      <TableCell> <strong> Account Created On </strong></TableCell>

                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {userList.map((users: User) => (
                      <TableRow key={users.id}>
                        <TableCell>{users.firstName} {users.lastName}</TableCell>
                        <TableCell>{users.email}</TableCell>
                        <TableCell>{users.points}</TableCell>
                        <TableCell>{users.role === 'user' ? 'Student' : users.role === 'admin' ? 'Admin' : users.role} </TableCell>
                        <TableCell>{formatDate(users.createdAt)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
        </Container >
        </Box >


      </main >
      <Footer />
    </ThemeProvider >
  );
}