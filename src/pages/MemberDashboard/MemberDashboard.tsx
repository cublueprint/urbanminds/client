import * as React from 'react';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CardHeader from '@mui/material/CardHeader';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import LinearProgress, {
  linearProgressClasses,
} from '@mui/material/LinearProgress';
import Box from '@mui/material/Box';
import { FormGroup, FormControlLabel, Switch, Link, CardActionArea, SvgIcon } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider, styled } from '@mui/material/styles';
import { Navigate, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { FormHelperText, TextField } from '@mui/material';
import Modal from '@mui/material/Modal';
import Stack from '@mui/material/Stack';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import ScheduleIcon from '@mui/icons-material/Schedule';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { SearchNavBar } from '../../components/SearchNavBar';
import { Opportunity } from '../../types/Opportunity';
import { OppurtunityDeletion } from '../../components/OpportunityDeletion';
import { useAuth } from '../../hooks/useAuth';
import { ErrorText } from '../../components/ErrorText';
import { EditOpportunityModal } from '../../components/EditOpportunityModal';
import { OpportunityModal } from '../../components/OpportunityModal';
import { RewardsModal } from '../../components/RewardsModal';
import { RewardRedemption } from '../../components/RewardRedemption/RewardRedemption';
import Radio from '@mui/material/Radio';
import { Email, Facebook, FacebookOutlined, Instagram, Edit } from '@mui/icons-material';
import { displayDateTime } from '../../utils';
import { Footer } from '../../components/Footer';

const RedeemButton = styled(Button)(({ theme }) => ({
  color: '#FFFFFF',
  backgroundColor: '#000000',
  '&:hover': {
    backgroundColor: '#ADADAD',
  },
}));


const RewardsPointsProgress = styled(LinearProgress)(({ theme }) => ({
  height: 30,
  borderRadius: 4,
  [`&.${linearProgressClasses.colorPrimary}`]: {
    backgroundColor: '#F2F2F2',
  },
  [`& .${linearProgressClasses.bar}`]: {
    borderRadius: 0,
    backgroundColor: '#FF8A00',
  },
}));

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

/** TODO: NEED TO MAKE A THEME COMPONENT  */
const theme = createTheme({
  palette: {
    text: {
      primary: '#1A1A1A',
      secondary: '#B2B2B2',
    },
    background: {
      default: '#F6F6F6',
    },
  },
  typography: {
    fontFamily: "'Helvetica Neue', Helvetica, Arial, sans-serif",
  },
});

// TODO: we should break this up into smaller components
export function MemberDashboard() {
  const [user, _, logout] = useAuth();
  const navigate = useNavigate();
  let query = '';
  const [searchQuery, setSearchQuery] = useState<string>(''); // add search query state variable
  const [active, setActive] = useState<boolean>(false);
  const [location, setLocation] = useState('');
  const accessToken = user?.tokens?.access.token;
  const [origOpps, setOrigOpps] = useState([]); // stores the original opportunities so that searches can be backtracked
  const [opps, setOpps] = useState<Opportunity[]>([]);
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [prizeModal, setPrizeModal] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [RedemptionModal, setRedemptionModal] = useState(false);

  const [points, setPoints] = useState(user ? user.points : 0);
  const [isClicked, setIsClicked] = useState<Opportunity | undefined>(undefined);
  let urlEndPoint = `${process.env.REACT_APP_API_ENDPOINT}/opportunities?active=${active}&sortBy=date`;

  const filterPosts = (posts, query) => {
    const formattedQuery = query.trim().toLowerCase();
    const tokens = formattedQuery.split(" ");

    if (!formattedQuery) {
      return posts;
    }

    return posts.filter(post => {
      const postName = post.title.toLowerCase();
      return tokens.some(token => postName.includes(token));
    });
  };

  const getChoice = choice => {
    query = choice;
    setOpps(filterPosts(origOpps, query));
    console.log(opps);
  };

  const handleCheckboxChange = e => {
    setActive(e.target.checked);
  };

  const handleLocationChange = e => {
    if (e.target.checked) {
      setLocation(e.target.value);
    } else {
      setLocation('');
    }
  };
  
  const fetchOpportunities = () => {
    urlEndPoint = `${process.env.REACT_APP_API_ENDPOINT}/opportunities?active=${active}&sortBy=date`;
    if (location) {
      urlEndPoint += `&location=${location}`;
    }

    fetch(urlEndPoint, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    })
      .then(async response => {
        if (!response.ok) {
          let res = await response.json();
          throw new Error(res.message);
        } else {
          return response.json();
        }
      })
      .then(data => {
        setOpps(data.results);
        setOrigOpps(data.results);
        console.log(opps);
      })
      .catch(err => {
        logout();
        console.log(err.message);
      });
  }
  
  useEffect(fetchOpportunities, [active, location]);

  const handleReset = () => {
    setActive(false);
    setLocation('');
    setOpps(origOpps);
  };

  const checkIsDisabled = (minimumPoints) => {
    return points < minimumPoints
  }

  const handleOpen = (id, edit) => {
    setIsClicked(opps.find(item => item.id === id));
    if (edit) {
      setOpenEdit(true);
    } else {
      setOpen(true);
    }
  };

  const handleOpenCreate = () => {
    setOpenCreate(true);
  }

  const handleCloseEdit = () => {
    setOpenEdit(false);
    fetchOpportunities();
  }

  const handleCloseCreate = () => {
    setOpenCreate(false);
    fetchOpportunities();
  }

  const handleClose = () => {
    setOpen(false);
  }

  const handlePrizeModal = () => {
    setPrizeModal(true);
  };

  const handleClosePrize = () => {
    setPrizeModal(false);
  };

  const handleRedemptionModal = () => {
    setRedemptionModal(true);
  };

  const handleChange = (event) => {
    //setModalValue(event.currentTarget.value);
  }

  if (!user || !user.tokens) {
    return <Navigate to="/login" />;
  }
  return (
    <ThemeProvider theme={theme}>
      <title>Member Dashboard</title>
      <CssBaseline />
      <SearchNavBar giveChoice={getChoice} />
      <main>
        {/* Hero unit */}
        <Container maxWidth="xl" sx={{ my: 8 }}>
          <Grid
            container
            my={3}
            spacing={{ xs: 2, xl: 4 }}
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item xs={12} sm={8} lg={5} >
              <Card
                // TODO: just trying to get commit to work
                sx={{
                  height: '300px',
                  p: 2,
                  display: 'flex',
                  flexDirection: 'column',
                  flex: 1,
                }}
              >
                <CardHeader
                  avatar={
                    <Avatar
                      alt="Urban Minds"
                      src="avatar.png"
                      sx={{ height: '80px', width: '80px'}}
                    />
                  }
                  title="Welcome back!"
                  titleTypographyProps={{ variant: 'h5', fontWeight: 'bold' }}
                  sx={{ p: 1 }}
                />
                <CardContent sx={{ flexGrow: 1, p: 1, display: 'flex', flexDirection: 'column', flex: 1 }}>
                  <Typography
                    paragraph
                    fontWeight='bold'
                    sx={{ marginBottom: 1 }}
                  >
                    {user.role === 'admin' ? "You are currently in admin view." : `You have ${points} reward points!`}
                  </Typography>
                  {user.role === 'admin' &&
                    <Typography
                      paragraph
                      sx={{ marginBottom: 1 }}
                    >
                      Manage opportunities, rewards, and users.
                    </Typography>
                  }
                  {user.role !== 'admin' &&
                    <Formik
                      initialValues={{ code: '' }}
                      validationSchema={Yup.object().shape({
                        code: Yup.string()
                          .required('Please submit a code to redeem')
                          .length(6)
                          .matches(/[\w-]/, 'Invalid code format'),
                      })}
                      onSubmit={(values, { setSubmitting, setErrors, resetForm, setStatus }) => {
                        setSubmitting(true);
                        const requestOptions: RequestInit = {
                          method: 'POST',
                          headers: {
                            'Content-Type': 'application/json',
                            Authorization: 'Bearer ' + accessToken,
                          },
                          body: JSON.stringify(values),
                        };
                        fetch(`${process.env.REACT_APP_API_ENDPOINT}/users/${user.id}/redeemCode`, requestOptions)
                          .then(async response => {
                            if (!response.ok) {
                              let res = await response.json();
                              throw new Error(res.message);
                            } else {
                              return response.json();
                            }
                          })
                          .then(data => {
                            setSubmitting(false);
                            setErrors({});
                            resetForm();
                            setStatus('Successfully redeemed!');
                            setPoints(data.points);
                          })
                          .catch(err => {
                            console.log(err.message);
                            setSubmitting(false);
                            setErrors({ code: err.message });
                            setStatus(null);
                          });
                      }}
                    >
                      {({
                        values,
                        touched,
                        status,
                        errors,
                        isSubmitting,
                        handleBlur,
                        handleChange,
                        handleSubmit,
                      }) => (
                        <form onSubmit={handleSubmit}>
                          <Stack spacing={1} direction="row-reverse" alignItems="center">
                            <Button type="submit" variant="contained" sx={{ width: '200px', outline: '#1A1A1A solid 2px', backgroundColor: '#1A1A1A', color: 'white' }}>
                              Redeem Code
                            </Button>
                            <TextField
                              name="code"
                              label="Event Code"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.code}
                              id="outlined"
                              size="small"
                              error={Boolean(errors.code)}
                              sx={{ marginBottom: '5px', input: { color: '#1A1A1A'  }}}
                              fullWidth
                            />
                          </Stack>
                          <FormHelperText>{errors.code}</FormHelperText>
                          {status &&
                            <Typography>{status}</Typography>
                          }
                        </form>
                      )}
                    </Formik>
                  }
                  <Stack spacing={1} direction="row" alignItems="center" sx={{ marginTop: 'auto' }}>
                    <Button onClick={handlePrizeModal} sx={{ outline: 'black solid 2px', backgroundColor: '#f18f36', color: 'black' }} variant="contained">
                      Rewards
                    </Button>
                    <>{user && user.role === "admin" &&
                      <Button onClick={handleRedemptionModal} variant="contained" sx={{ marginTop: '15px', outline: 'black solid 2px', backgroundColor: '#f18f36', color: 'black'  }}>
                        Edit Reward Redemptions
                      </Button>
                    }</>
                  </Stack>
                </CardContent>
              </Card>
            </Grid>
            <Grid item xs={0} sm={4} lg={7} >
              <Card
                sx={{
                  height: '300px',
                  display: { xs: 'none', md: 'flex' }
                }}
              >
                <CardMedia
                  component="img"
                  height="300px"
                  image="https://i.ibb.co/6wxXxLH/1-UP-Conference-Sign-up-Image.jpg"
                  alt="random"
                  sx={{ objectPosition: '50% 30%' }}
                />
              </Card>
            </Grid>
          </Grid>
          <Stack mt={8} mb={5} spacing={2} direction="row" alignItems='center'>
            <Typography
              variant="h5"
              fontWeight="bold"
              align="center"
              color="text.primary"
              mr={3}
            >
              Opportunities for you
            </Typography>
            <FormControlLabel
              control={
                <Switch
                  checked={active}
                  onChange={handleCheckboxChange}
                  color="primary"
                />
              }
              label="Active"
              sx={{ ml: 1 }}
            />
            <FormGroup sx={{ display: 'flex', flexDirection: 'row' }}>
              <FormControlLabel
                control={
                  <Radio
                    checked={location === ''}
                    value=""
                    onChange={handleLocationChange}
                  />
                }
                label="All locations"
                onChange={handleLocationChange}
              />
              <FormControlLabel
                control={
                  <Radio
                    checked={location === 'Toronto'}
                    value="Toronto"
                    onChange={handleLocationChange}
                  />
                }
                label="Toronto"
                onChange={handleLocationChange}
              />
              <FormControlLabel
                control={
                  <Radio
                    checked={location === 'Abashiri'}
                    value="Abashiri"
                    onChange={handleLocationChange}
                  />
                }
                label="Abashiri"
                onChange={handleLocationChange}
              />
              <FormControlLabel
                control={
                  <Radio
                    checked={location === 'Online'}
                    value="Online"
                    onChange={handleLocationChange}
                  />
                }
                label="Online"
                onChange={handleLocationChange}
              />
            </FormGroup>
            {user && user.role === 'admin' &&
              <Button variant="contained" onClick={handleOpenCreate} sx={{ outline: 'black solid 2px', backgroundColor: '#f18f36', color: 'black', marginLeft: '10px' }}>
                Create Opportunity
              </Button>
            }
          </Stack>
          <Grid container spacing={4}>
            {opps.map((opp: Opportunity) => (
              <Grid item xs={12} sm={6} lg={3}>
                <Card
                  sx={{
                    height: '100%',
                  }}
                >
                  <CardActionArea
                    sx={{  }}
                    onClick={() => handleOpen(opp['id'], false)}>
                    <CardMedia
                      component="img"
                      height="300px"
                      image={opp.image ? opp.image.location : 'placeholder.png'}
                      sx={{ objectFit: 'cover' }}
                      alt="Opportunity image"
                    />
                    <CardContent sx={{ display: "flex", flexDirection: "column" }}>
                      <Typography fontWeight="bold" fontSize={20} sx={{ marginBottom: "15px" }}>
                        {opp['title']}
                      </Typography>
                      {/* <Link
                        component="button"
                        paragraph
                        fontWeight="bold"
                        fontSize={20}
                        textAlign="left"
                        underline="hover"
                        color={'#1A1A1A'}
                        onClick={() => {
                          handleOpen(opp['id'], false)
                        }}
                        marginBottom="20px"
                      >
                        {opp['title']}
                      </Link>
                      */}
                      <Stack direction="row" alignItems="center" gap={.5} sx={{ marginBottom: "5px" }}>
                        <CalendarMonthIcon fontSize="inherit"/>
                        <Typography variant="body2" fontWeight="bold">
                          {displayDateTime(opp['startDate'])}  
                        </Typography>
                      </Stack>
                      <Stack direction="row" alignItems="center" gap={.5}>
                        <LocationOnIcon fontSize="inherit"/>
                        <Typography variant="body2">
                          {opp['location']}
                        </Typography>
                      </Stack>
                      <Typography
                        paragraph
                        variant="body2"
                        sx={{
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          display: "-webkit-box",
                          WebkitLineClamp: "2",
                          WebkitBoxOrient: "vertical",
                          marginTop: "15px",
                        }}>
                        {opp['description']}
                      </Typography>
                    </CardContent>
                    <Box
                      sx={{
                        borderRadius: 3,
                        border: '2px solid black',
                        backgroundColor: 'white',
                        position: 'absolute',
                        top: '10px',
                        left: '10px',
                        height: '36px',
                        paddingX: '10px',
                        display: 'flex',
                        alignItems: 'center',
                      }}
                    >
                      <Typography variant="body2" fontWeight="bold">
                        {opp['points']} points
                      </Typography>
                    </Box>
                    {user.role === 'admin' && (
                      <Box
                        sx={{
                          position: 'absolute',
                          right: '10px',
                          top: '10px',
                        }}
                      >
                        <Button
                          onClick={e => {
                            handleOpen(opp['id'], true);
                            e.stopPropagation();
                          }}
                          variant="contained"
                          color="info"
                          sx={{ marginRight: '6px' }}
                        >
                          <Edit />
                        </Button>
                        <OppurtunityDeletion
                          authToken={user.tokens.access.token}
                          opportunity={opp}
                          opportunityArray={opps}
                          opportunitySetState={setOpps}
                          buttonStyles={{}}
                        />
                      </Box>
                    )}
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
        <RewardsModal open={prizeModal} handleClose={handleClosePrize} accessToken={accessToken} points={points} setPoints={setPoints} />
        <OpportunityModal isClicked={isClicked} open={open} handleClose={handleClose} accessToken={accessToken} />
        <EditOpportunityModal isClicked={isClicked} openEdit={openEdit} handleCloseEdit={handleCloseEdit} accessToken={accessToken} />
        <EditOpportunityModal isClicked={undefined} openEdit={openCreate} handleCloseEdit={handleCloseCreate} accessToken={accessToken} />
        <Modal open={RedemptionModal} onClose={() => setRedemptionModal(false)}>
          <Card sx={{
              width: { xs: '90%', xl: '60%'},
              height: { xs: '90%', xl: '80%'},
              position: 'fixed',
              top: '50%',
              left: '50%',
              transform: 'translate(-50%, -50%)',
              borderRadius: 3
            }}>
            <RewardRedemption authToken={user.tokens.access.token} />
          </Card>
        </Modal>
      </main >
      <Footer />
    </ThemeProvider >
  );
}
