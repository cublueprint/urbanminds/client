import React from 'react';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import { GlobalStyle } from './styles/GlobalStyle';

import { HomePage } from './pages/HomePage';
import { LoginPage } from './pages/LoginPage';
import { Register } from './pages/Register';
import { MemberDashboard } from './pages/MemberDashboard';
import { ProfilePage } from './pages/ProfilePage';
import { UsersListPage } from './pages/UsersListPage';

import { NotFoundPage } from './components/NotFoundPage';

function App() {
  console.log(process.env.REACT_APP_API_ENDPOINT)
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<Register />} />
        <Route path="/dashboard" element={<MemberDashboard />} />
        <Route path="/profile" element={<ProfilePage />} />
        <Route path="/userslist" element={<UsersListPage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
      <GlobalStyle />
    </BrowserRouter>
  );
}

export default App;
