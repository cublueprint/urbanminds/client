import { useNavigate } from 'react-router-dom';
import { useLocalStorage } from './useLocalStorage';

export type User = {
  birthDate: string;
  createdAt: string;
  email: string;
  firstName: string;
  lastName: string;
  id: string;
  isEmailVerified: boolean;
  pastInvolvement: string;
  school: string;
  points: number;
  tokens: {
    access: {
      expires: string;
      token: string;
    };
    refresh: {
      expires: string;
      token: string;
    };
  };
  redeemedCodes: [string];
  updatedAt: string;
  role: string;
  opportunities: [string];
};

type LoginData = {
  user: User;
  tokens: User['tokens'];
};

export function useAuth(): [User | null, (data: LoginData) => void, () => void] {
  const [user, setUser] = useLocalStorage<User | null>('user', null);
  const navigate = useNavigate();

  // call this function when you want to authenticate the user
  const login = (data: LoginData) => {
    let user = data.user;
    user.tokens = data.tokens;
    setUser(user);
    navigate('/dashboard');
  };

  // call this function to sign out logged in user
  const logout = () => {
    setUser(null);
    navigate('/login', { replace: true });
  };


  // for now, if the access token expires then log out
  //  TODO: in the future, we want to use refresh tokens to get new access tokens
  if (user && user.tokens && new Date(user.tokens.access.expires) < new Date()) {
    logout();
  }

  return [user, login, logout];
};
