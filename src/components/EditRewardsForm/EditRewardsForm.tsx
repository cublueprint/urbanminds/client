import React from 'react';
import { useState, ChangeEvent, FormEvent } from 'react';

export function EditRewardsForm(): React.ReactElement {
  const [inputs, setInputs] = useState<{ [key: string]: string }>({});
  const [name, setName] = useState<string>("");

  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({ ...values, [name]: value }));
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    alert(JSON.stringify(inputs));
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Enter your name:
        <input
          type="text"
          name="username"
          value={name}
          onChange={handleChange}
        />
      </label>

      <input type="submit" />
    </form>
  );
}

