import React from 'react';
import { Delete } from '@mui/icons-material';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';

import { Opportunity } from '../../types';

interface Props {
  authToken: string;
  opportunity: Opportunity;
  opportunityArray: Opportunity[];
  opportunitySetState: React.Dispatch<React.SetStateAction<Opportunity[]>>;
  buttonStyles: object;
}

export function OppurtunityDeletion({
  opportunity,
  opportunityArray,
  opportunitySetState,
  buttonStyles,
  authToken,
}: Props): JSX.Element {
  const [open, setOpen] = React.useState<boolean>(false);
  const [error, setError] = React.useState<string>('');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setError('');
  };

  const handleConfirm = () => {
    const requestOptions: RequestInit = {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    };
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/opportunities/${opportunity.id}`, requestOptions)
      .then(async (response) => {
        if (!response.ok) {
          let res = await response.json();
          throw new Error(res.message);
        }
        return response;
      })
      .then((data) => {
        console.log(data);
        handleClose();
        opportunitySetState(
          opportunityArray.filter(({ id }) => id !== opportunity.id)
        );
      })
      .catch((err) => {
        setError('There was an error deleting the opportunity.');
        console.log(err.message);
      });
  };

  return (
    <>
      <Button
        onClick={handleClickOpen}
        variant="contained"
        color="error"
        sx={buttonStyles}
      >
        <Delete />
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>
          Are you sure you want to delete this opportunity?
        </DialogTitle>
        <DialogContent>
          <i>"{opportunity.title}"</i> will be permanently deleted. Please
          confirm that you'd like to delete this opportunity.
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleClose} autoFocus>
            Cancel
          </Button>
          <Button variant="contained" color="error" onClick={handleConfirm}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
