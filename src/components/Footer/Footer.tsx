import { Email, Facebook, Instagram } from "@mui/icons-material";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Stack from "@mui/material/Stack";
import SvgIcon from "@mui/material/SvgIcon";
import Typography from "@mui/material/Typography";

const TikTokIcon = ({ color = "#000000" }) => {
  return (
    <SvgIcon>
      <svg
        fill={color}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 50 50"
      >
        <path d="M41,4H9C6.243,4,4,6.243,4,9v32c0,2.757,2.243,5,5,5h32c2.757,0,5-2.243,5-5V9C46,6.243,43.757,4,41,4z M37.006,22.323 c-0.227,0.021-0.457,0.035-0.69,0.035c-2.623,0-4.928-1.349-6.269-3.388c0,5.349,0,11.435,0,11.537c0,4.709-3.818,8.527-8.527,8.527 s-8.527-3.818-8.527-8.527s3.818-8.527,8.527-8.527c0.178,0,0.352,0.016,0.527,0.027v4.202c-0.175-0.021-0.347-0.053-0.527-0.053 c-2.404,0-4.352,1.948-4.352,4.352s1.948,4.352,4.352,4.352s4.527-1.894,4.527-4.298c0-0.095,0.042-19.594,0.042-19.594h4.016 c0.378,3.591,3.277,6.425,6.901,6.685V22.323z" />
      </svg>
    </SvgIcon>
  );
};

export function Footer() {
  return (
    <Box sx={{ bgcolor: 'background.paper', p: 6, flex: 1, display: 'flex', flexDirection: 'row', alignItems: 'center' }
    } component="footer" >
      <Stack direction="column" gap={1} sx={{ flexGrow: 1 }}>
        <Typography sx={{ textDecoration: 'none' }}>Powered by</Typography>
        <Link href="https://www.urbanminds.co/" color='inherit' >
          <img
            src="UMlogo.png"
            alt="logo"
            style={{
              height: '40px',
            }}
          />
        </Link>
      </Stack>
      <Stack direction="column" justifyContent="flex-end">
        <Stack direction="row" gap={1} justifyContent="flex-end">
          <Link href="https://www.facebook.com/urbanmindsTO/?ref=notif&notif_t=page_user_activity&notif_id=1484514599286716">
            <Facebook sx={{ color: '#c5cbcf' }} />
          </Link>
          <Link href="https://www.tiktok.com/@1uptoronto">
            <TikTokIcon color='#c5cbcf' />
          </Link>
          <Link href="https://www.instagram.com/urbanmindsto/?hl=en">
            <Instagram sx={{ color: '#c5cbcf' }} />
          </Link>
          <Link href="mailto:hello@urbanminds.co">
            <Email sx={{ color: '#c5cbcf' }} />
          </Link>
        </Stack>
        <Typography variant="caption">© COPYRIGHT {new Date().getFullYear()}. ALL RIGHTS RESERVED.</Typography>
      </Stack>
    </Box >
  )
}