import { ThemeProvider, createTheme } from '@mui/material';
import Typography from '@mui/material/Typography';

const theme = createTheme({
  palette: {
    text: {
      primary: '#1A1A1A',
      secondary: '#B2B2B2',
    },
  },
  typography: {
    fontFamily: "'Helvetica Neue', Helvetica, Arial, sans-serif",
  },
});

export function NotFoundPage() {
  return (
    <ThemeProvider theme={theme}>
      <main>
        <Typography variant="h3" style={{ color: 'black' }}>
          404 Page Not Found
        </Typography>
      </main>
    </ThemeProvider>
  );
}