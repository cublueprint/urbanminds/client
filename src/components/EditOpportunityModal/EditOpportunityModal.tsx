import React, { useEffect } from 'react';
import { Opportunity } from '../../types';
import Modal from '@mui/material/Modal';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import { Formik } from 'formik';
import * as Yup from 'yup';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import { ErrorText } from '../ErrorText';
import Stack from '@mui/material/Stack';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import FormHelperText from '@mui/material/FormHelperText';
import Button from '@mui/material/Button';
import dayjs, { Dayjs } from 'dayjs';
import { DateTimePicker } from '@mui/x-date-pickers';
import { Edit, Delete, UploadFile, CloudUpload, Restore } from '@mui/icons-material';
import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import Snackbar from '@mui/material/Snackbar';
import Alert, { AlertProps } from '@mui/material/Alert';
import { LinearProgress, Typography } from '@mui/material';

interface UpdateOpportunity {
  title?: string;
  description?: string;
  eventLink?: string;
  startDate?: Date | Dayjs | null;
  location?: string;
  points?: number;
  capacity?: number;
  organizationName?: string;
  eventType?: string;
  school?: string;
}

interface Props {
  isClicked: Opportunity | undefined;
  openEdit: boolean;
  handleCloseEdit: () => void;
  accessToken: string | undefined;
}

const VisuallyHiddenInput = styled('input')({
  clip: 'rect(0 0 0 0)',
  clipPath: 'inset(50%)',
  height: 1,
  overflow: 'hidden',
  position: 'absolute',
  bottom: 0,
  left: 0,
  whiteSpace: 'nowrap',
  width: 1,
});

// type Image = string | ArrayBuffer

export function EditOpportunityModal({ isClicked, openEdit, handleCloseEdit, accessToken }: Props): JSX.Element {
  const [snackbar, setSnackbar] = React.useState<Pick<
    AlertProps,
    'children' | 'severity'
  > | null>(null);

  const handleCloseSnackbar = () => setSnackbar(null);

  const [submitError, setSubmitError] = React.useState('');
  const [image, setImage] = React.useState(null);
  const [displayImage, setDisplayImage] = React.useState<string|null>((isClicked && isClicked.image) ? isClicked.image.location : null);

  useEffect(() => {
    setImage(null);
    setDisplayImage((isClicked && isClicked.image) ? isClicked.image.location : null);
  }, [openEdit]);

  const handleImageChange = e => {
    if (!e.target.files || e.target.files.length === 0) {
      return;
    }

    const file = e.target.files[0];

    if (file.size > 50000000) {
      setSnackbar({ children: `Image too large (max 50 MB)`, severity: 'error' });
      return;
    }
    
    setImage(file);
    setDisplayImage(URL.createObjectURL(file));

    // Preview the selected image
    // const reader = new FileReader();
    // reader.onloadend = () => {
    //   setDisplayImage(reader.result);
    // };
    // reader.readAsDataURL(file);
    // if (e.target.files && e.target.files[0]) {
    //   setImage(URL.createObjectURL(event.target.files[0]));
    // }
  }

  const handleImageDelete = _ => {
    setImage(null);
    setDisplayImage(null);
  }

  const handleImageReset = _ => {
    setImage(null);
    setDisplayImage((isClicked && isClicked.image) ? isClicked.image.location : null);
  }

  return (
    <Modal open={openEdit} onClose={handleCloseEdit}>
      <>
        <Card
          sx={{
            width: { xs: '90%', xl: '60%'},
            height: { xs: '90%', xl: '80%'},
            position: 'fixed',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            flexDirection: 'row',
            borderRadius: 3
          }}
        >
          <Box
            sx={{
              position: 'absolute',
              left: '10px',
              top: '10px',
            }}
          >
            <Button
              component="label"
              variant="contained"
              color="info"
              sx={{ marginRight: '6px' }}
            >
              <CloudUpload />
              <VisuallyHiddenInput type="file" onChange={handleImageChange} accept="image/png, image/jpeg, image/gif"/>
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={handleImageDelete}
              sx={{ marginRight: '6px' }}
            >
              <Delete />
            </Button>
            <Button
              variant="contained"
              color="inherit"
              onClick={handleImageReset}
              sx={{ backgroundColor: 'darkgray' }}
            >
              <Restore />
            </Button>
          </Box>
          <CardMedia
            component="img"
            height="100%"
            image={displayImage || 'placeholder.png'}
            alt="Opportunity image"
            sx={{ resizeMode: 'contain', width: '55%', marginRight: '10px' }}
          />
          {(image || (isClicked && isClicked['image'] && !displayImage)) &&
            <Typography
              variant="body1"
              sx={{
                display: "inline",
                position: "absolute",
                top: "45%",
                width: "55%",
                textAlign: "center",
                color: "black",
                backgroundColor: "white",
                fontWeight: "bold",
              }}
            >
              {image ? "Image preview only. Save changes to upload." : "Image preview only. Save changes to delete."}
            </Typography>
          }
          <CardContent sx={{ flexGrow: 1, overflow: 'auto' }}>
            <Formik validateOnChange={false}
              validateOnBlur={false}
              initialValues={isClicked ? {
                title: isClicked['title'].toString(),
                description: isClicked['description'],
                eventLink: isClicked['eventLink'],
                startDate: dayjs(isClicked['startDate']),
                location: isClicked['location'],
                points: isClicked['points'],
                capacity: isClicked['capacity'],
                organizationName: isClicked['organizationName'],
                eventType: isClicked['eventType'],
                school: isClicked['school'],
              } : {
                title: "",
                description: "",
                eventLink: "",
                startDate: dayjs(Date.now()),
                location: "",
                points: 0,
                capacity: 0,
                organizationName: "",
                eventType: "",
                school: "",
              }}
              validationSchema={Yup.object().shape({
                title: Yup.string()
                  .required('Title required.'),
                description: Yup.string()
                  .required('Description required.'),
                eventLink: Yup.string()
                  .required('Event link required.'),
                startDate: Yup.date()
                  .typeError('Invalid date entered')
                  .required('Date required.'),
                location: Yup.string()
                  .required('Location required.'),
                points: Yup.number()
                  .required('Points required.')
                  .typeError('Please enter a number'),
                capacity: Yup.number()
                  .required('Capacity required.')
                  .typeError('Please enter a number'),
                organizationName: Yup.string()
                  .required('Organization name required.'),
                eventType: Yup.string()
                  .required('Event type required.'),
                school: Yup.string(),
              })}
              onSubmit={async (values: UpdateOpportunity, { setSubmitting }) => {
                setSubmitting(true);
                const updatedOpp: UpdateOpportunity = {
                  title: values.title,
                  description: values.description,
                  eventLink: values.eventLink,
                  startDate: values.startDate,
                  location: values.location,
                  points: values.points,
                  capacity: values.capacity,
                  organizationName: values.organizationName,
                  eventType: values.eventType,
                  school: values.school,
                };
                console.log(updatedOpp);
                
                const updateRequestOptions: RequestInit = {
                  method: isClicked ? 'PATCH' : 'POST',
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + accessToken,
                  },
                  body: JSON.stringify(updatedOpp),
                };
                const updateUrl = `${process.env.REACT_APP_API_ENDPOINT}/opportunities/${isClicked ? isClicked['id'] : ""}`
                
                try {
                  const opportunityResponse = await fetch(updateUrl, updateRequestOptions);

                  const opportunityData = await opportunityResponse.json();
                  if (!opportunityResponse.ok) {
                    throw new Error(opportunityData.message);
                  }


                  // if image exists at all, then update
                  // isClicked['image'] exists and displayImage is none, then delete
                  let imageRequestMethod;
                  if (image) {
                    imageRequestMethod = 'POST';
                  } else if (isClicked && isClicked['image'] && !displayImage) {
                    imageRequestMethod = 'DELETE';
                  } else {
                    imageRequestMethod = null;
                  }

                  if (imageRequestMethod) {
                    const formData = new FormData();
                    if (image)
                      formData.append('image', image);

                    const imageRequestOptions: RequestInit = {
                      method: imageRequestMethod,
                      headers: {
                        Authorization: 'Bearer ' + accessToken,
                      },
                      body: formData,
                    };
                    const imageUrl = `${process.env.REACT_APP_API_ENDPOINT}/opportunities/${opportunityData.id}/image`

                    const imageResponse = await fetch(imageUrl, imageRequestOptions);

                    const imageData = await imageResponse.json();
                    if (!imageResponse.ok) {
                      throw new Error(imageData.message);
                    }

                    setDisplayImage(imageData.image ? imageData.image.location : null);
                  }

                  handleCloseEdit();
                } catch (err: any) {
                  setSubmitError(err.message);
                } finally {
                  setSubmitting(false);
                }
              }}
            >
              {({
                values,
                touched,
                errors,
                isSubmitting,
                handleBlur,
                handleChange,
                handleSubmit,
                setFieldValue,
              }) => (
                <form onSubmit={handleSubmit}>
                  <Stack direction='column' spacing={1} sx={{ py: 2, pr: 2 }}>
                      <TextField
                        name="title"
                        label="Title"
                        inputProps={{ style: { fontWeight: 'bold' } }}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="outlined"
                        size="small"
                        value={values.title}
                        error={Boolean(errors.title && touched.title)}
                        fullWidth
                        sx={{ marginTop: '15px', width: "100%" }}
                      />
                      <ErrorText error={touched.title ? errors.title : ''} />
                      <TextField
                        name="description"
                        label="Description"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="outlined"
                        size="small"
                        value={values.description}
                        multiline={true}
                        error={Boolean(errors.description && touched.description)}
                        sx={{ marginTop: '15px', width: "100%" }}
                      />
                      <ErrorText error={touched.description ? errors.description : ''} />
                      <TextField
                        name="eventLink"
                        label="Event Link"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="outlined"
                        size="small"
                        value={values.eventLink}
                        error={Boolean(errors.eventLink && touched.eventLink)}
                        sx={{ marginTop: '15px', width: "100%" }}
                      />
                      <ErrorText error={touched.eventLink ? errors.eventLink : ''} />
                      <TextField
                        name="points"
                        label="Points"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="outlined"
                        size="small"
                        value={values.points}
                        error={Boolean(errors.points && touched.points)}
                        sx={{ marginTop: '15px', width: "100%" }}
                      />
                      <ErrorText error={touched.points ? errors.points : ''} />
                      <Stack direction="row" alignItems="center" gap={.5} sx={{ marginTop: "15px" }}>
                        <LocationOnIcon />
                        <TextField
                          name="location"
                          label="Location"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          id="outlined"
                          size="small"
                          value={values.location}
                          error={Boolean(errors.location && touched.location)}
                          sx={{ marginTop: '15px', width: "100%" }}
                        />
                      </Stack>
                      <ErrorText error={touched.location ? errors.location : ''} />
                      <Stack direction="row" alignItems="center" gap={.5} sx={{ marginTop: "15px" }}>
                        <CalendarMonthIcon />
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                          <DateTimePicker
                            label="Start Date/Time"
                            onChange={value => setFieldValue('startDate', value)}
                            value={values.startDate}
                            slotProps={{ textField: {
                                name: "startDate",
                                margin: "normal",
                                required: true,
                                onBlur: handleBlur,
                                error: Boolean(errors.startDate && touched.startDate)
                              }
                            }}
                          />
                        </LocalizationProvider>
                      </Stack>
                      <ErrorText error={touched.startDate ? errors.startDate : ''} />
                      <TextField
                        name="capacity"
                        label="Capacity"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="outlined"
                        size="small"
                        value={values.capacity}
                        error={Boolean(errors.capacity && touched.capacity)}
                        sx={{ marginTop: '15px', width: "100%" }}
                      />
                      <ErrorText error={touched.capacity ? errors.capacity : ''} />
                      <TextField
                        name="organizationName"
                        label="Organization Name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="outlined"
                        size="small"
                        value={values.organizationName}
                        error={Boolean(errors.organizationName && touched.organizationName)}
                        sx={{ marginTop: '15px', width: "100%" }}
                      />
                      <ErrorText error={touched.organizationName ? errors.organizationName : ''} />
                      <TextField
                        name="eventType"
                        label="Event Type"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="outlined"
                        size="small"
                        value={values.eventType}
                        error={Boolean(errors.eventType && touched.eventType)}
                        sx={{ marginTop: '15px', width: "100%" }}
                      />
                      <ErrorText error={touched.eventType ? errors.eventType : ''} />
                      <TextField
                        name="school"
                        label="School"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="outlined"
                        size="small"
                        value={values.school}
                        error={Boolean(errors.school && touched.school)}
                        sx={{ marginTop: '15px', width: "100%" }}
                      />
                      <ErrorText error={touched.school ? errors.school : ''} />
                      {submitError &&
                        <ErrorText error={submitError} />
                      }
                      <Button type="submit" disabled={isSubmitting} variant="contained" sx={{ marginTop: '0px' }}>
                        Save Changes
                      </Button>
                      {isSubmitting &&
                        <LinearProgress />
                      }
                  </Stack>
                </form>
              )}
            </Formik>

          </CardContent>
        </Card>
        {!!snackbar && (
          <Snackbar
            open
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            onClose={handleCloseSnackbar}
            autoHideDuration={6000}
          >
            <Alert {...snackbar} onClose={handleCloseSnackbar} />
          </Snackbar>
        )}
      </>
    </Modal>
  );
}
