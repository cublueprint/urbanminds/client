import React from 'react';
import { LinearProgress, linearProgressClasses } from '@mui/material';

import zxcvbn from 'zxcvbn';

interface Props {
  password: string;
  validity: object;
}

export function PasswordStrengthBar({ password, validity }: Props): JSX.Element {
  // zxcvbn is a really cheap calculation, so we can do it on every render
  const { score } = zxcvbn(password);
  const valid = passwordIsValid(validity);

  return (
    <>
      <LinearProgress
        variant="determinate"
        value={getBarValue(score, valid)}
        sx={{
          [`&.${linearProgressClasses.colorPrimary}`]: {
            backgroundColor: 'lightGray',
          },
          [`& .${linearProgressClasses.bar}`]: {
            backgroundColor: getBarColor(score, valid),
          },
          color: 'primary',
          height: 5,
          borderRadius: 5,
        }}
      />
    </>
  );
}

function getBarValue(score: zxcvbn.ZXCVBNScore, valid: boolean): number {
  // Lowering the upper bound because we don't want a full bar for an invalid password
  return valid ? score * (100 / 4) : score * (80 / 4);
}

function getBarColor(score: zxcvbn.ZXCVBNScore, valid: boolean): string {
  // Color can't be fully green if the password isn't valid even if it's strong
  if (score === 4 && valid) {
    return 'limegreen';
  }
  if (score >= 3) {
    return 'yellowgreen';
  }
  if (score === 2) {
    return 'orange';
  }
  return 'red';
}

function passwordIsValid(validity: object): boolean {
  return Object.values(validity).every((value) => value === true);
}
