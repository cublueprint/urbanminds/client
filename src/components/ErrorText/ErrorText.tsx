import { FormHelperText } from "@mui/material";
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';

const PREFIX = 'Register';

const classes = {
  error: `${PREFIX}-error`,
  hidden: `${PREFIX}-hidden`
};

export const StyledButton = styled(Button)({
  [`& .${classes.error}`]: {
    display: 'block',
    marginLeft: 8,
  },
  [`& .${classes.hidden}`]: {
    display: 'block',
  },
});

interface ErrorTextProps {
  error: any | undefined;
}

export const ErrorText = ({ error }: ErrorTextProps) => (
  <FormHelperText
    error={Boolean(error)}
    className={error ? classes.error : classes.hidden}
  >
    {error}
  </FormHelperText>
);

