import React, { useState, useEffect } from 'react';
//import '../../../styles/style.css';
import { Button, Table, TableHead, TableRow, TableCell, TableBody } from '@mui/material';

interface Redemption {
    id: string;
    userId: string;
    rewardId: string;
    fulfilled: boolean;
}



export const RewardRedemption = ({ authToken }) => {
    const [redemptions, setRedemptions] = useState<Redemption[]>([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/redemptions`, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    })
    .then(response => response.json())
    .then(data => {
        console.log(data);
        setRedemptions(data);
    })
    .catch(err => console.error(err));
  }, []);

  const fulfillRedemption = (redemptionId) => {
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/redemptions/${redemptionId}/fulfill`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    })
    .then(response => {
      if (!response.ok) throw new Error('Network response was not ok');
      return response.json();
    })
    .then(updatedRedemption => {
        if (Array.isArray(redemptions) && updatedRedemption && typeof updatedRedemption === 'object') {
            setRedemptions(redemptions.map(r => r.id === updatedRedemption.id ? updatedRedemption : r));
          } else {
            console.error('Invalid data format');
          }
    })
    .catch(err => console.error(err));
  };

  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>User ID</TableCell>
          <TableCell>Reward ID</TableCell>
          <TableCell>Fulfilled</TableCell>
          <TableCell>Actions</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {redemptions.map((redemption) => (
          <TableRow key={redemption.id}>
            <TableCell>{redemption.userId}</TableCell>
            <TableCell>{redemption.rewardId}</TableCell>
            <TableCell>{redemption.fulfilled ? 'Yes' : 'No'}</TableCell>
            <TableCell>
              {!redemption.fulfilled && (
                <Button onClick={() => fulfillRedemption(redemption.id)}>Mark as Fulfilled</Button>
              )}
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};
