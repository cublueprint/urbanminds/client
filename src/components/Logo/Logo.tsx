import Grid from '@mui/material/Grid'

export function Logo(props: any) {
  return (
    <Grid container wrap="nowrap" {...props}>
      <Grid item className="sm-logo-rectangle1" sx={{ mr: 0.3 }}></Grid>
      <Grid item className="sm-logo-rectangle2" sx={{ mr: 0.3 }}></Grid>
      <Grid item className="sm-logo-rectangle3"></Grid>
    </Grid>
  );
}
