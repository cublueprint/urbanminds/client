import React from 'react';
import { useState, ChangeEvent } from 'react';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import InputBase from '@mui/material/InputBase';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import SettingsIcon from '@mui/icons-material/Settings';
import Button from '@mui/material/Button';
import { Logo } from '../Logo';
import { useAuth } from '../../hooks/useAuth';
import { Link } from 'react-router-dom';
import Grid from '@mui/material/Grid';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  border: '2px solid #1A1A1A',
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: theme.spacing(2),
  width: '100%',
  [theme.breakpoints.up('md')]: {
    marginLeft: theme.spacing(3),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    // padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '50ch',
    },
  },
}));

// TODO: the type of props should be defined
export function SearchNavBar(props: any) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [user, login, logout] = useAuth();

  const isMenuOpen = Boolean(anchorEl);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'left',
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <Link to="/dashboard" style={{ textDecoration: 'none',color: 'inherit'  }}>
        <MenuItem onClick={handleMenuClose}>Dashboard</MenuItem>
      </Link>

      <Link to="/profile" style={{ textDecoration: 'none',color: 'inherit'  }}>
        <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      </Link>
      <>{user && user.role === "admin" &&
        <Link to="/userslist" style={{ textDecoration: 'none',color: 'inherit'  }}>
          <MenuItem onClick={handleMenuClose}>List of Users</MenuItem>
        </Link>
      }</>
      <MenuItem onClick={logout}>Logout</MenuItem>
    </Menu >
  );

  const [searchInput, setSearchInput] = useState('');

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    setSearchInput(e.target.value);
    props.giveChoice(e.target.value);
  };

  return (
    <AppBar position="static" color="transparent" elevation={0}>
      <Container maxWidth="xl">
        <Toolbar sx={{ py: 3, }} disableGutters>
          {/* logo */}
          <Link to="/dashboard">
            <Grid container>
              <img
                src="1UPLogo.png"
                alt="logo"
                style={{
                  height: '35px',
                  marginLeft: '1',
                  marginRight: '1',
                }}
              />
            </Grid>
          </Link>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              id="search_input"
              inputProps={{ 'aria-label': 'search' }}
              onChange={handleChange}
              value={searchInput}
            />
          </Search>

          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: 'flex', mx: 1 }}>
            <Button
              variant="outlined"
              size="large"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
              sx={{
                borderRadius: 5,
                py: 0.5,
                px: 1,
                boxShadow: '0 3px 3px rgba(0, 0, 0, 0.25)',
              }}
            >

              <Avatar
                alt="Urban Minds"
                src="avatar.png"
                sx={{ width: 30, height: 30, mr: 0.5 }}
              />
              <SettingsIcon sx={{ fontSize: 25 }} />
            </Button>
          </Box>
        </Toolbar>{' '}
        {renderMenu}
      </Container>
    </AppBar>
  );
}
