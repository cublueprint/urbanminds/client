import Typography from '@mui/material/Typography';
import React from 'react';

interface PasswordValidity {
  minChar: boolean;
  number: boolean;
  lower: boolean;
  upper: boolean;
  specialChar: boolean;
}

interface PasswordStrengthIndicatorProps {
  validity: PasswordValidity;
}

export function PasswordStrengthIndicator({
  validity: { minChar, number, lower, upper, specialChar },
}: PasswordStrengthIndicatorProps): JSX.Element {
  return (
    <div className="text-left mb-4">
      <Typography
        sx={{
          fontSize: '16px',
          lineHeight: '138%',
          color: '#B3B3B3',
        }}
      >
        Password must contain:
      </Typography>
      <ul className="text-muted">
        <PasswordStrengthIndicatorItem isValid={minChar} text="Have at least 8 characters" />
        <PasswordStrengthIndicatorItem isValid={number} text="Have at least 1 number" />
        <PasswordStrengthIndicatorItem isValid={lower} text="Have at least 1 lower-case letter" />
        <PasswordStrengthIndicatorItem isValid={upper} text="Have at least 1 upper-case letter" />
        <PasswordStrengthIndicatorItem isValid={specialChar} text="Have at least 1 special character" />
      </ul>
    </div>
  );
}

interface PasswordStrengthIndicatorItemProps {
  isValid: boolean | null;
  text: string;
}

const PasswordStrengthIndicatorItem = ({
  isValid,
  text,
}: PasswordStrengthIndicatorItemProps): JSX.Element => {
  const highlightClass = isValid
    ? 'text-success'
    : isValid !== null
    ? 'text-danger'
    : '';
  return <li className={highlightClass}>{text}</li>;
};
