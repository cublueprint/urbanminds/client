import { useEffect, useState } from "react";
import Modal from "@mui/material/Modal";
import { Opportunity } from "../../types/Opportunity";
import { User, useAuth } from "../../hooks/useAuth";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import LocationOnIcon from '@mui/icons-material/LocationOn';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { ErrorText } from "../ErrorText";
import { List, ListItem, ListItemText, ListItemButton } from "@mui/material";
import { displayDateTime } from "../../utils";
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import { Link as LinkIcon } from "@mui/icons-material";
import { Link } from "@mui/material";
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';


interface Props {
  isClicked: Opportunity | undefined;
  open: boolean;
  handleClose: () => void;
  accessToken: string | undefined;
}

export function OpportunityModal({ isClicked, open, handleClose, accessToken }: Props): JSX.Element {
  const [user, _1, _2] = useAuth();
  const [participated, setParticipated] = useState<boolean>(false);

  const getRegistrationStatus = () => {
    if (!user || !isClicked)
      return;

    const requestOptions = {
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    }

    fetch(`${process.env.REACT_APP_API_ENDPOINT}/users/${user.id}`, requestOptions)
      .then(async response => {
        if (!response.ok) {
          let res = await response.json();
          throw new Error(res.message);
        } else {
          return response.json();
        }
      })
      .then(data => {
        console.log(data.opportunities)
        const hasParticipated = data.opportunities && data.opportunities.includes(isClicked['id'])
        setParticipated(hasParticipated);
      })
  }

  useEffect(getRegistrationStatus, [isClicked]);

  return (
    <Modal open={open} onClose={handleClose}>
      <>{isClicked &&
        <Card
          sx={{
            width: { xs: '90%', xl: '60%'},
            height: { xs: '90%', xl: '80%'},
            position: 'fixed',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            display: 'flex',
            flexDirection: 'row',
            borderRadius: 3
          }}
          // sx={{ height: '500px', width: '900px', top: '25%', left: '25%', display: 'flex', flexDirection: 'row', position: 'relative', borderRadius: 3 }}
        >
          <CardMedia
            component="img"
            height="100%"
            image={isClicked.image ? isClicked.image.location : 'placeholder.png'}
            alt="Opportunity image"
            sx={{ resizeMode: 'contain', width: '60%', marginRight: '10px' }}
          />
          <CardContent sx={{ display: 'block', overflow: 'auto' }}>
            <Typography gutterBottom variant="h5" component="h2" sx={{ fontWeight: "bold" }}>
              {isClicked['title']}
            </Typography>
            <Box
              sx={{
                borderRadius: 3,
                border: '2px solid black',
                backgroundColor: 'white',
                height: '36px',
                paddingX: '10px',
                marginY: '15px',
                display: 'inline-flex',
                alignItems: 'center',
              }}
            >
              <Typography variant="body2" fontWeight="bold">
                {isClicked['points']} points
              </Typography>
            </Box>
              {participated &&
                <Stack direction="row" alignItems="center" gap={.5} sx={{ marginBottom: "10px", marginTop: "10px" }}>
                  <CheckCircleOutlineIcon sx={{ color: 'green' }} />
                  <Typography sx={{ fontWeight: 'bold', color: 'green' }}>
                    You've participated in this event!
                  </Typography>
                </Stack>
              }
            <Stack direction="row" alignItems="center" gap={.5} sx={{ marginBottom: "10px", marginTop: "10px" }}>
              <LinkIcon />
              <Link
                href={isClicked['eventLink']}
                target="_blank"
                sx={{ fontWeight: "bold" }}
              >
                Register on Eventbrite
              </Link> 
            </Stack>
            <Stack direction="row" alignItems="center" gap={.5} sx={{ marginBottom: "10px", marginTop: "10px" }}>
              <LocationOnIcon />
              <Typography>{isClicked['location']}</Typography>
            </Stack>
            <Stack direction="row" alignItems="center" gap={.5} sx={{ marginBottom: "10px", marginTop: "10px" }}>
              <CalendarMonthIcon />
              <Typography sx={{ marginBottom: "10px", marginTop: "10px" }}>
                {displayDateTime(isClicked['startDate'])}
              </Typography>
            </Stack>

            <Typography sx={{ marginBottom: "10px", marginTop: "10px" }}>
              {isClicked['description']}
            </Typography>
            {user && user.role === 'admin' && isClicked['users'] &&
              <>
                <Stack direction="row" alignItems="center" gap={.5} sx={{ marginBottom: "10px", marginTop: "10px" }}>
                  <Typography><b>Code</b></Typography>
                  <Typography>{isClicked['code']}</Typography>
                </Stack>
                <Stack direction="column" gap={.5} sx={{ marginBottom: "10px", marginTop: "10px" }}>
                  <Typography variant="h6">Participants</Typography>
                  {isClicked['users'].length ?
                    <List sx={{ border: '1px solid #555', maxHeight: '180px', overflowY: 'scroll' }}>
                      {isClicked['users'].map(user => {
                        return (
                          <>
                            <ListItem sx={{ paddingX: '10px', paddingY: 0 }}>
                              <ListItemText primary={user.firstName + ' ' + user.lastName} secondary={user.email} />
                            </ListItem>
                            <Divider component="li" />
                          </>
                        )
                      })}
                    </List> :
                    <Typography>No participants found.</Typography>
                  }
                </Stack>
              </>
            }
          </CardContent>
        </Card>
      }</>
    </Modal>
  );
}