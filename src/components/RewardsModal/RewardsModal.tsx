import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import RedeemIcon from '@mui/icons-material/Redeem';
import {
  GridRowsProp,
  GridRowModesModel,
  GridRowModes,
  DataGrid,
  GridColDef,
  GridToolbarContainer,
  GridActionsCellItem,
  GridEventListener,
  GridRowId,
  GridRowModel,
  GridRowEditStopReasons,
} from '@mui/x-data-grid';
import { useAuth } from '../../hooks/useAuth';
import Modal from '@mui/material/Modal';
import Card from '@mui/material/Card';
import Alert, { AlertProps } from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import { ObjectId } from 'bson';
import { CardContent, CardHeader, Typography } from '@mui/material';

interface EditToolbarProps {
  setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void;
  setRowModesModel: (
    newModel: (oldModel: GridRowModesModel) => GridRowModesModel,
  ) => void;
  accessToken: string | undefined;
}

interface Reward {
  name: string;
  points: number;
  id: GridRowId;
  isNew: boolean;
}

function EditToolbar(props: EditToolbarProps) {
  const { setRows, setRowModesModel, accessToken } = props;

  const handleClick = () => {
    // const id = new ObjectId().toHexString();
    const id = Date.now();
    setRows((oldRows) => [...oldRows, { id, name: '', points: 0, isNew: true }]);
    setRowModesModel((oldModel) => ({
      ...oldModel,
      [id]: { mode: GridRowModes.Edit, fieldToFocus: 'name' },
    }));
  };

  return (
    <GridToolbarContainer>
      <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
        Add reward
      </Button>
    </GridToolbarContainer>
  );
}


export function RewardsModal({ open, handleClose, accessToken, points, setPoints }) {
  const initialRows: GridRowsProp = [];
  const [rows, setRows] = React.useState(initialRows);
  const [user, _1, logout] = useAuth();

  const [rowModesModel, setRowModesModel] = React.useState<GridRowModesModel>({});

  const fetchRewards = () => {
    const urlEndPoint = `${process.env.REACT_APP_API_ENDPOINT}/rewards`;

    fetch(urlEndPoint, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    })
      .then(async response => {
        if (!response.ok) {
          let res = await response.json();
          throw new Error(res.message);
        } else {
          return response.json();
        }
      })
      .then(data => {
        setRows(data);
      })
      .catch(err => {
        logout();
      });
  }

  React.useEffect(fetchRewards, []);

  const useMutation = (reward: Partial<Reward>) => {
    return new Promise<Partial<Reward>>((resolve, reject) => {
      setTimeout(() => {
        const shouldUpdate = !reward.isNew;
        const requestOptions = {
          method: shouldUpdate ? 'PATCH' : 'POST',
          headers: {
            Authorization: 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ name: reward.name, points: reward.points }),
        };

        fetch(`${process.env.REACT_APP_API_ENDPOINT}/rewards/${shouldUpdate ? reward.id : ""}`, requestOptions)
          .then(async response => {
            if (!response.ok) {
              let res = await response.json();
              throw new Error(res.message);
            } else {
              return response.json();
            }
          })
          .then(async data => {
            resolve(data)
          })
          .catch(err => {
            reject(new Error(err.message));
          })
      }, 200);
    });
  }

  const [snackbar, setSnackbar] = React.useState<Pick<
    AlertProps,
    'children' | 'severity'
  > | null>(null);

  const handleCloseSnackbar = () => setSnackbar(null);

  const processRowUpdate = async (newRow: GridRowModel) => {
    const response = await useMutation(newRow);
    const updatedRow = { ...response, isNew: false };
    setSnackbar({ children: 'Reward successfully saved', severity: 'success' });
    const index = rows.findIndex((row) => row.id === newRow.id);
    setRows(oldRows => [...oldRows.slice(0, index), updatedRow, ...oldRows.slice(index + 1)])
    return updatedRow;
  };

  const handleProcessRowUpdateError = (error: Error) => {
    setSnackbar({ children: `Reward could not be created/updated: ${error.message}`, severity: 'error' });
  };


  const handleRowEditStop: GridEventListener<'rowEditStop'> = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };

  const handleRedeemClick = (id: GridRowId) => () => {
    if (!user) {
      return
    }
    const requestOptions = {
      method: 'PATCH',
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    };
    

    fetch(`${process.env.REACT_APP_API_ENDPOINT}/users/${user.id}/redeemReward/${id}`, requestOptions)
      .then(async response => {
        if (!response.ok) {
          let res = await response.json();
          throw new Error(res.message);
        } else {
          return response.json();
        }
      })
      .then(data => {
        setPoints(data.points);
        setSnackbar({ children: 'Reward successfully redeemed! UM staff will reach out soon.', severity: 'success' });
      })
      .catch(err => {
        setSnackbar({ children: `Reward could not be redeemed: ${err.message}`, severity: 'error' });
      });
  }

  const handleEditClick = (id: GridRowId) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleSaveClick = (id: GridRowId) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
  };

  const handleDeleteClick = (id: GridRowId) => () => {
    const requestOptions = {
      method: 'DELETE',
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    };

    fetch(`${process.env.REACT_APP_API_ENDPOINT}/rewards/${id}`, requestOptions)
      .then(async response => {
        if (!response.ok) {
          let res = await response.json();
          throw new Error(res.message);
        } else {
          return response;
        }
      })
      .then(data => {
        setRows(rows.filter((row) => row.id !== id));
        setSnackbar({ children: 'Reward successfully deleted.', severity: 'success' });
      })
      .catch(err => {
        setSnackbar({ children: `Reward could not be deleted: ${err.message}`, severity: 'error' });
      })
  };

  const handleCancelClick = (id: GridRowId) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });

    const editedRow = rows.find((row) => row.id === id);
    if (editedRow!.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };

  const handleRowModesModelChange = (newRowModesModel: GridRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  const columns: GridColDef[] = [
    { field: 'name', headerName: 'Name', width: 180, editable: true },
    {
      field: 'points',
      headerName: 'Points',
      type: 'number',
      width: 80,
      align: 'left',
      headerAlign: 'left',
      editable: true,
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Actions',
      width: 100,
      cellClassName: 'actions',
      getActions: ({ id }) => {
        if (!user || user.role !== 'admin') {
          return [
            <GridActionsCellItem
              icon={<RedeemIcon />}
              label="Redeem"
              sx={{
                color: 'primary.main',
              }}
              onClick={handleRedeemClick(id)}
            />,
          ]
        }
        const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: 'primary.main',
              }}
              onClick={handleSaveClick(id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(id)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  const editToolbar = (user && user.role === 'admin') ? {
    slots: {
      toolbar: EditToolbar,
    },
    slotProps: {
      toolbar: { setRows, setRowModesModel, accessToken },
    }
  } : {};

  const subtitle = (user && user.role !== 'admin') ? `Redeem your ${points} points for any of the following rewards!` : ""

  return (
    <Modal open={open} onClose={handleClose}>
      <Card
        sx={{
          width: { xs: '90%', xl: '60%'},
          height: { xs: '90%', xl: '80%'},
          position: 'fixed',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          display: 'flex',
          flexDirection: 'column',
          borderRadius: 3
        }}
      >
        <CardHeader title="Rewards" subheader={subtitle} />
          <DataGrid
            rows={rows}
            columns={columns}
            editMode="row"
            rowModesModel={rowModesModel}
            onRowModesModelChange={handleRowModesModelChange}
            onRowEditStop={handleRowEditStop}
            processRowUpdate={processRowUpdate}
            onProcessRowUpdateError={handleProcessRowUpdateError}
            {...editToolbar}
          />
        {!!snackbar && (
          <Snackbar
            open
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            onClose={handleCloseSnackbar}
            autoHideDuration={6000}
          >
            <Alert {...snackbar} onClose={handleCloseSnackbar} />
          </Snackbar>
        )}
      </Card>
    </Modal>
  );
}
